// $Id$

Anonymous / Authenticated Page Views displays the unique number of node page views by authenticated users and page views by anonymous users. It does not set a cookie to track unique anonymous node page views.

To use, just copy to sites/all/modules and enable on the module administration page.